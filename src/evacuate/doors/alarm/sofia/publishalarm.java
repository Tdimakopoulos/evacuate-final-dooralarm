package evacuate.doors.alarm.sofia;

import com.indra.sofia2.ssap.kp.implementations.rest.SSAPResourceAPI;
import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import com.indra.sofia2.ssap.kp.implementations.rest.resource.SSAPResource;
import eu.evacuate.og.ws.client.post.CreateOperations;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.exus.timestamp.timestamp;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Thomas Dimakopoulos eVACUATE_WP8SensorsStatus
 *
 */
public class publishalarm {

    static String szURL = "http://192.168.8.232:8080/sib/services/api_ssap/";
    static SSAPResourceAPI p = null;
    static String sessionkey;
    static SSAPResource pres = new SSAPResource();

    public void DeleteFile(String filepath) {
        try {

            File file = new File("c:\\evacctrl\\" + filepath);

            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
//                System.out.println("Delete operation is failed.");
            }

        } catch (Exception e) {

//            e.printStackTrace();

        }
    }

    public boolean FileExists(String filePathString) {
        File f = new File("c:\\evacctrl\\" + filePathString);
        if (f.exists() && !f.isDirectory()) {
            return true;
        }
        return false;
    }

    public void close() {
        pres.setLeave(true);
        p.delete(pres);
    }

    public void writeFile1Line(String file, String line) throws IOException {
        FileWriter fw = new FileWriter("c:\\evacctrl\\" + file);

        fw.write(line);

        fw.close();
    }

    //SocialKPOnly
    //77bcfc0c6ddb4c92bc9f3d463cea27e6
    public void PublishAlarm() throws ResponseMapperException {
        if (p == null) {
            p = new SSAPResourceAPI(szURL);
        }

        pres.setJoin(true);

        pres.setToken("77bcfc0c6ddb4c92bc9f3d463cea27e6");
        pres.setInstanceKP("SocialKPOnly:SocialKPOnlyIns");

        Response presponse = p.insert(pres);
        if (presponse.getStatus() == 200) {
            //good
            sessionkey = p.responseAsSsap(presponse).getSessionKey();
            System.out.print("Session Key : ");
            System.out.println(sessionkey);
        } else {
            System.out.println("error : " + presponse.getStatus());
        }

    }

    public void QueryDoorsOnto() throws IOException, ResponseMapperException, JSONException {
        Response presponse = p.query(sessionkey, "eVACUATE_GEOAIABMS_Status", "select * from eVACUATE_GEOAIABMS_Status ORDER by contextData.timestamp desc limit 100", "", "SQLLIKE");

        System.out.println("Doors Update : " + presponse.getStatus());
        String dd = p.responseAsSsap(presponse).getData().toString();

        JSONArray inputArray = new JSONArray(dd);
        System.out.println("Count " + inputArray.length());

        for (int i = 0; i < inputArray.length(); i++) {
            System.out.println("-->");
            JSONObject jo = inputArray.getJSONObject(i);
            JSONObject jo2 = jo.getJSONObject("GEOAIABMS_Status");
//System.out.println("-->"+jo2);
            String stype = jo2.getString("type");
            String sstatus = jo2.getString("status");
            String ident = jo2.getString("identifier");

            if (stype.equalsIgnoreCase("GREEN_BOX_DOOR_BUTTON")) {
                if (sstatus.equalsIgnoreCase("ALARM")) {
                    System.out.println("---------------------------------------------------------> GBDB");

                    if (FileExists(ident)) {
                    } else {
                        writeFile1Line(ident, ident);
                        CreateOperations pop = new CreateOperations();
//                    pop.SendSMMToAll("Door Alarm");
                        pop.SendMessageToTetra("3786", "Door Alarm");
                    }
                }
                else
                {
                    DeleteFile(ident);
                }
            }

            if (stype.equalsIgnoreCase("GREEN_BOX_DOOR_KEY")) {
                if (sstatus.equalsIgnoreCase("UNLOCKED")) {
                    System.out.println("---------------------------------------------------------> GBDK");
                    System.out.println("SMS");
                    if (FileExists(ident)) {
                    } else {
                        writeFile1Line(ident, ident);
                        CreateOperations pop = new CreateOperations();
//                    pop.SendSMMToAll("Door Unlock");
                        pop.SendMessageToTetra("3786", "Door Unlock");
                    }
                }
                else
                {
                    DeleteFile(ident);
                }
            }

            if (stype.equalsIgnoreCase("MAGNETIC_DOOR")) {
                if (sstatus.equalsIgnoreCase("Alarm")) {
                    System.out.println("---------------------------------------------------------> MD");
                    if (FileExists(ident)) {
                    } else {
                        writeFile1Line(ident, ident);
                        CreateOperations pop = new CreateOperations();
//                    pop.SendSMMToAll("Magnetic Door Alarm");
                        pop.SendMessageToTetra("3786", "Magnetic Door Alarm");
                    }
                }else
                {
                    DeleteFile(ident);
                }
            }

            System.out.println(stype);
            System.out.println(sstatus);
            System.out.println(ident);
        }

    }

    public void writeFile2(String file, String line) throws IOException {
        FileWriter fw = new FileWriter("c:\\evacctrl\\" + file);

        fw.write(line);

        fw.close();
    }

}
