/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evacuate.door.alarms;

import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import evacuate.doors.alarm.sofia.publishalarm;
import java.io.File;
import java.io.IOException;
import org.primefaces.json.JSONException;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class EvacuateDoorAlarms {

    
    public boolean FileExists(String filePathString) {
        File f = new File("c:\\evacctrl\\"+filePathString);
        if (f.exists() && !f.isDirectory()) {
            return true;
        }
        return false;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ResponseMapperException, IOException, JSONException, InterruptedException {
        System.out.println("Starting Evacuate Start/End Server !");
        publishalarm pa = new publishalarm();
        pa.PublishAlarm();
        EvacuateDoorAlarms psms=new EvacuateDoorAlarms();
        
        
        while (!psms.FileExists("door.stop")) {
            pa.QueryDoorsOnto();
            Thread.sleep(10000);
        }
    }
    
}
